Buongiorno a tutte e tutti!

Grazie di essere venuti, e grazie di ricevermi. Sono grata agli organizzatori
(la facoltà, il dipartimento) per avermi dato l'opportunità di promuovere il
software libero. È la prima volta che lo faccio in italiano, e questo mi fa
molto piacere.

Come diceva Maurizio in premessa, mi chiamo Marianne Corvellec. Sono francese e
canadese. Di solito, parlo in francese o in inglese. Ma posso anche parlare in
italiano perché da bambina ho vissuto a Pisa. Tuttavia, ho dimenticato
moltissimo, quindi perdonate i miei errori di pronuncia o di grammatica! Vorrei
ringraziare Marie-Odile e Isabella, dell'April, per avere riletto il mio testo.

Come indicato nel titolo, l'intervento si svolgerà in tre parti: prima parte --
l'università, seconda parte -- la comunità, terza parte -- la vita pubblica,
per quanto riguarda il software libero. Prima di iniziare, sapete cos'è il
[software libero](http://www.gnu.org/philosophy/free-sw.it.html)?

Un software è libero se, e solo se, gli utenti di questo programma, di questo
software possono godere di quattro libertà fondamentali:

- numero zero, la libertà di eseguire il programma come si desidera;
- numero uno, la libertà di studiare il programma e di adattarlo (per quello
c'è bisogno del codice sorgente);
- numero due, la libertà di ridistribuire copie per aiutare gli altri;
- numero tre, la libertà di migliorare il programma e distribuirne versioni
modificate (così la comunità ne può beneficiare).

Suppongo e spero che molti di voi usiate del software libero: per esempio,
Mozilla Firefox (per navigare sul Web) o il linguaggio Python (utilizzato nel
calcolo scientifico)...? E anche un sistema operativo libero, come GNU/Linux!
Il software che non è libero si chiama proprietario.

Forse lo sapete già, oppure ne avete l'intuizione adesso, ma il software libero
non è soltanto una questione tecnica. Non si tratta solo di software, di
computer, di informatica. Anzi, si tratta di libertà, di giustizia e, se
vogliamo, di comunità e di democrazia. C'è un movimento internazionale intorno
al software libero, che è un vero e proprio movimento di giustizia sociale,
poiché la nostra lotta si svolge contro il controllo, la dominazione, la
manipolazione degli utenti da parte del software proprietario (autori, editori,
venditori).

Dunque, comincio con la prima parte. Non vi preoccupate, avremo tempo per le
domande e i commenti dopo l'intervento.
