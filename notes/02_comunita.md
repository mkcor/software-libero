Bene. Adesso, parliamo della comunità o, piuttosto, delle comunità,
considerando tutte le unioni e intersezioni possibili. Dal punto di vista
tecnico, io conosco sopratutto le comunità [SciPy](https://scipy.org/)
(pacchetti scientifici Python) e [Tidyverse](https://www.tidyverse.org/)
(pacchetti R per la "data science").

Però, con il FLOSS, c'è sempre qualcosa di più, più sociale e culturale che
tecnico. Ho appena detto un acronimo famoso: FLOSS. Significa "Free/Libre and
Open Source Software" (in inglese, sebbene "libre" si a mutuato al francese; si
può altresì prendere la parola, spagnola o italiana, "libero"). In inglese,
"free" significa sia "libero" che "gratuito"; quindi si aggiunge spesso il
termine "libre" per specificare che "free" si riferisce alla libertà e non al
prezzo.

Mi fermo qui per un minuto al fine di chiarire una confusione molto diffusa.
Nella comunità del software libero, ci sono due visioni politiche. C'è il
movimento del software libero e quello dell'open source. Il primo si preoccupa
della libertà degli utenti informatici (è una preoccupazione di ordine etico,
morale, filosofico); il secondo si concentra sugli aspetti tecnici e pratici
(per esempio, lo sviluppo del software sarà più efficiente e il codice di
migliore qualità).

Io sono un'attivista del movimento del software libero (sono membro della FSF 
-- Free Software Foundation, sono membro dell'April e persino del suo Consiglio
di Amministrazione), quindi dico "software libero" il più possibile. Ma sono
anche una scienziata e contribuisco ad alcuni progetti dove la gente vuole
essere neutrale (sulla questione politica). Se si vuole quindi essere neutrali
e chiari nella scelta tra software libero ed open source, il modo per farlo è
dire [“FLOSS”](https://www.gnu.org/philosophy/floss-and-foss.it.html). Ecco per
il vocabolario.

Stavo dicendo che con il software libero, c'è sempre qualcosa di più grande che
la sola funzione tecnica. Per esempio, conoscete Matplotlib, la libreria Python
per fare grafici? A partire dalla versione 1.5, ci sono mappe di colori che
sono percettivamente uniformi. E a partire dalla
[versione 2.0](https://matplotlib.org/users/dflt_style_changes.html#colormap),
la mappa "viridis" (che varia dal blu scuro al giallo chiaro) è la mappa di
colori predefinita (di "default"). Vogliamo usare una mappa di colori
percettivamente uniforme, così una varizione da 0.1 a 0.2 nei dati si vede, si
percepisce nei colori come una variazione da 0.8 a 0.9 (è lo stesso "delta").

Quindi, questa modifica, questo miglioramento è una buona cosa dal punto di
vista scientifico e tecnico. Nella maggior parte dei casi (per il "default"),
vogliamo inoltre che la mappa sia sequenziale, stampabile in bianco e nero, e
anche carina (perché no?). Devo ammettere che questa sensibilità estetica è
qualcosa che mi piace nei progetti FLOSS. Ciascuno poteva pronunciarsi nella
comunità; c'è stato un voto per scegliere la nuova mappa di colori predefinita
-- in sostituzione di "jet" che non è carina, e sopratutto non è
percettivamente uniforme, e non va bene per le persone
[daltoniche](http://matplotlib.org/users/colormaps.html#colorblindness)... Si
doveva rimediare a questa mancanza. Quindi, il cambiamento da "jet" a "viridis"
è una buona cosa anche dal punto di vista dell'inclusione. E ne sono
orgogliosa!

Quello che apprezzo nelle comunità FLOSS (quelle che conosco e amo) è il
contesto benevolo e l'approccio umano. Non è tradizionalmente così nelle
comunità accademiche... Però l'ondata di
[Open Science](http://ivory.idyll.org/blog/2016-what-is-open-science.html) e di
ricerca riproducibile dovrebbe ripulire le cattive prassi del vecchio mondo. Vi
invito ad informarvi sulle prassi di ricerca riproducibile.

Per illustrare la benevolenza, sopratutto verso i novellini, prendo l'esempio 
del pacchetto (o libreria) [yt](http://yt-project.org/), perché ci sto
lavorando attualmente. Yt è un programma per la visualizzazione di dati
scientifici multidimensionali (e grandi). Le indicazioni per
[contribuire](https://github.com/yt-project/yt#contributing) al progetto
cominciano con un "Imposter syndrome disclaimer", dicendo "We want your help.
No, really." È un'avvertenza rispetto alle persone che potrebbero sentirsi
inesperte (si chiama il sindrome dell'impostore). Tipicamente, gli scienzati si
sentono impostori in programmazione (ed includo me stessa).

Un altro slogan di yt è "Community: yt's best feature." Infatti, si trova del
l'aiuto online, sia come utente, sia come contributore. E vorrei sottolineare
la definizione molto ampia e aperta di "comunità" nel caso di yt. I membri
della comunità non sono soltanto dei contributori al codice. Sono tutte le
persone che si interessano abbastanza al progetto per dare la loro opinione.

Il mese scorso, a Montreal, si è svolta la DebConf17 -- la conferenza su
Debian, il sistema operativo libero molto diffuso, alla base di Ubuntu, ecc. Le
presentazioni erano bellissime e l'ambiante molto piacevole. Ci sentivamo a
nostro agio. Questo è dovuto in parte al codice di condotta. Un codice di
condotta è uno strumento (ma solo uno strumento) che può essere utile per
costruire una comunità accogliente. Per esempio, in tantissime comunità FLOSS,
ci sono pochissime donne. Chiaramente si deve fare qualcosa. Le "Carpentries"
([Software Carpentry](https://software-carpentry.org/),
[Data Carpentry](http://www.datacarpentry.org/),
[Library Carpentry](https://librarycarpentry.github.io/)) hanno fatto un lavoro
[splendido](http://www.datacarpentry.org/blog/how-we-wrote-our-code-of-conduct/)
e adoro questa comunità. Datele un'occhiata, se non conoscete questa comunità 
dedicata all'insegnamento di conoscenze informatiche per la ricerca.

Ogni comunità deve cercare e scegliere le proprie soluzioni. Queste soluzioni,
poi, possono ispirare altre comunità, e lo scambio di idee fa nascere un
dialogo ricco e vantaggioso, sulle esperienze di successo o di fallimento
relativo. Credo che ogni comunità debba rimanere sovrana. Vi ricordo che
l'informatica libera si propone, non s'impone. Analogicamente, non mi piaciono
le tendenze imperialistiche. Negli ultimi anni c'è stata una spinta per
introdurre codici di condotta in ogni evento Python (o collegato). Mi sembra
che questo formalismo rifletta la cultura nord-americana, che predomina nelle
comunità FLOSS. E, ancora una volta, un codice di condotta può essere molto
utile, può dare risultati molto positivi. Ma rimane uno strumento per
realizzare un obiettivo più generale di inclusione, di uguaglianza, di umanità.

Si possono fare tante azioni positive senza codice di condotta. La settimana
scorsa ero in Grecia per insegnare alla scuola estiva
["Advanced Scientific Programming in Python"](https://python.g-node.org). Gli
organizzatori di questa scuola estiva (era la decima edizione) non utilizzano
nessun codice di condotta, però hanno intrapreso azioni che hanno condotto alla
parità donne-uomini tra gli studenti.

Le questioni di sovranità, di governo ci portano naturalmente alla parte tre,
sulla vita pubblica.
