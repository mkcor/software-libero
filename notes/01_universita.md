Io non ci sono più, all'università. Però, ci ho passato diversi anni. Ho
conseguito un dottorato in fisica statistica. Ho cominciato i miei studi
universitari in geologia, scienze della terra, scienze naturali. Ho frequentato
vari corsi di matematica per poi alla fine studiare e fare ricerca nei campi
della fisica fondamentale, fisica matematica. Come voi, giusto?

Così, in informatica, mi considero (quasi) autodidatta. E ho davvero potuto (e
voluto) imparare un po' di programmazione, reti, Web grazie al software libero
in quanto tale (tramite le libertà numero uno e due) e grazie alle comunità del
software libero (come ambiente di apprendimento, scambio e condivisione).

Adesso lavoro come "data scientist" al "Centro di Ricerca Informatica di
Montréal", un centro di ricerca e di competenza informatica finanziato al 50%
dal governo provinciale del Québec. L'altra metà viene generata da contratti di
Ricerca e Sviluppo, o di accompagnamento tecnologico con varie Piccole e Medie
Imprese, grandi imprese, enti e organismi.

Quindi, torno all'università, e riprendo le parole che ho pronunciato un anno
fa, al Forum sociale mondiale 2016 -- a Montréal (era la prima volta che un
Forum sociale mondiale si svolgeva in un paese del "Nord"). Lì, ho avuto il
privilegio di intervenire assieme a Richard Stallman. Richard Stallman fondò il
[progetto GNU](https://www.gnu.org/gnu/thegnuproject.it.html) e la
[Free Software Foundation](https://www.fsf.org/) negli anni ottanta.

All'università (Scuola Normale Superiore di Lione, in realtà), facevo delle
simulazioni numeriche e dei grafici scientifici. Immagino che sia anche il
vostro caso. Però, usavo (tra l'altro) un software proprietario: per ignoranza,
per mimetismo -- perché questo programma aveva colonizzato  i computer, i
corsi, il dipartimento di fisica, gli insegnanti, le menti.

Che sfortuna! O che fortuna, poiché questo mi ha portata all'esperienza
fondatrice... della privazione della libertà. Mi è successo intorno al 2010.
Non potevo più lanciare il programma a causa di problemi di licenza. E anche se
avessi potuto risolvere questo problema di licenza ed eseguire il programma,
non avrei potuto verificare (o, più modestamente, studiare, esaminare) come
questa o quella operazione di algebra lineare veniva implementata...

Come si può fare ricerca, per principio, con scatole nere? Ciò è contrario ad
ogni metodo scientifico. Ebbene, mi sono rivolta al software libero, e ho
trovato un approccio corretto, onesto, trasparente, rispettoso (della propria
libertà, della propria intelligenza), partecipativo e molto eccitante perché,
infatti, tutto è possibile! Svilupperò nella parte due, sulla comunità.

Vorrei introdurre il problema dei brevetti software. Come mai? In primo luogo,
perché costituiscono il pericolo maggiore per il software libero. Nella parte
tre, sulla vita pubblica, spiegherò che anzi costituiscono un pericolo per
tutti gli sviluppatori di software (libero o no).

Attualmente si parla molto di "innovazione". E le università devono dare il
loro contributo. Ma che cosa vuol dire, innovazione? Significa... depositare
brevetti? Spero di no! Ma possiamo chiedercelo, quando sentiamo i politici
tradizionali, alcuni responsabili universitari, la retorica dominante in
generale.

Vorrei pertanto chiarire alcuni punti. Immagino che, anche se non siete
giuristi (non lo sono neanch'io), avete sentito parlare dei brevetti come cose
che dovrebbero proteggere le invenzioni. Quindi, un inventore può depositare
una domanda di brevetto presso uno stato o un gruppo di stati. In Europa, lo
farà presso l'UEB (Ufficio europeo dei brevetti), che copre 38 stati europei.

Faccio una pausa per dire che io adesso mi esprimo solo per quanto riguarda i
brevetti *software* (non parlo di brevetti *in generale*). E parlo solo dei
brevetti, non parlo di Copyright né di Trademark (marchio commerciale).
Purtroppo, questi tre elementi vengono spesso raggruppati sotto il termine
"proprietà intellettuale". Questo termine è parziale, nebuloso, crea confusione
e, non so cosa ne pensate, ma per me è un ossimoro.

Quindi, depositi una domanda di brevetto, e l'UEB (o l'agenzia in questione)
deve valutare il carattere innovativo della tua invenzione. Questa valutazione
viene effettuata in modo neutrale, imparziale, competente, illuminato? Capite
che se riteniamo "brevetti == innovazione == bene", la tendenza sarà di
concedere brevetti, più brevetti, sempre più brevetti. Il caso dell'UEB è
ancora più chiaro, ancore più evidente, poiché non è un'agenzia governativa,
è un'organizzazione autonoma -- finanziariamente.

Quindi, deve generare redditi, aumentare i redditi, e questi redditi provengono
direttamente e unicamente dalle domande di brevetto. I richiedenti di brevetti
sono i clienti dell'UEB, e l'UEB ha interesse a far piacere ai propri clienti,
rilasciando brevetti, più brevetti, sempre più brevetti. Quest'organizzazione è
giudice e parte! Vi invito ad avere questo in mente, quando farete la vostra
ricerca applicata, le vostre start-up (universitarie), le vostre spin-off (a
partire dai vostri laboratori di eccellenza). Io lavorato in tre start-up nella
mia carriera, e mi è piaciuto tantissimo.

Per concludere questa prima parte, volevo citare Bernard Stiegler (un filosofo
francese) e la sua associazione
[Ars Industrialis](http://arsindustrialis.org/). Loro hanno idee rinfrescanti
sul ruolo possibile (e desiderabile) delle università negli "ecosistemi"
dell'innovazione, e ve le raccomando. Propongono l'idea di un'*internazione*
delle università...
