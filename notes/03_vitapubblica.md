Non sono la persona più esperta per parlare del governo delle società umane.
(Mia sorella ne sa molto di più!) Però cominciamo da qualche parte. È come il
software libero: costruiamo il futuro una linea di codice alla volta. In questa
piccola frase si vede un progetto e un metodo, il quale ricorda i "commons"
(beni comuni).

Conoscete i commons? I beni comuni ci servono e anzi costituiscono un
patrimonio (per l'umanità, oppure per una piccola comunità). Oggi non parlo
dell'importanza di fare piccole azioni; ne ho parlato un po' al Forum sociale
mondiale 2016 (è disponibile su Internet, in francese, però). Personalmente ho
una relazione molto artigianale con il codice. E voglio vedere la mia pratica
come un pezzettino nella grande tela della civilizzazzione. È proprio questo
concetto che ci rende umani.

A questo proposito, vi invito a scoprire il progetto
[Software Heritage](https://www.softwareheritage.org/) fondato da Roberto di
Cosmo e da Stefano Zacchiroli. (Tra parentesi, Stefano è stato il Debian Lead
alcuni anni fa, ed era alla DebConf il mese scorso -- sempre un piacere!)
Potrebbe essere utile per la vostra ricerca e, inoltre, ha un valore di
patrimonio culturale.

In realtà, il concetto dei commons (dico sempre la parola inglese, perché
la traduzione "beni comuni" non è affatta perfetta; non sono i "common
goods"... comunque) oppure altri concetti di patrimonio sviluppati dall'Unesco
sono molto più adatti alla dimensione collettiva, che mi interessa, e che va
nel senso di costruire una comunità ed un futuro in comune.

Il progetto GNU è il primo esempio di "digital commons" (commons digitali),
benché le licenze libere (quelle che ti danno le quattro libertà) non
comportino niente di collettivo. Il software libero non contiene
intrinsecamente le idee di comunità, di progetto collettivo. Forse riflette le
origini americane "libertarie" (però, "libertarian", nel senso individualista)
del software libero.

E se vogliamo dargli una portata collettiva, *libertaria* nel senso anarchico
originale francese (Joseph Déjacque, nel 1857), dobbiamo farlo noi. E credo che
ne valga la pena. Mi sembra che il software libero sia un esempio intorno a cui
si possa creare l'emancipazione individuale, l'economia della contribuzione, la
trasformazione sociale, ecc. (Solo questo!)

Dunque, è importantissimo salvaguardare il software libero, che adesso viene
minacciato su alcuni fronti, fra cui, in primo luogo, i brevetti software. A
titolo aneddotico, questa battaglia contro i brevetti software è un po'
speciale per me, perché la campagna contro il progetto di brevetto unitario
europeo nel 2012 era la mia prima esperienza di campagna con l'April.

L'[April](https://april.org/) è la più grande associazione di promozione e di
difesa del software libero nel mondo francofono. L'April esiste dal 1996 e
conta più di 4 000 membri. Siamo molto vicini alla FSF. Nel Consiglio di
Amministrazione dell'April siamo 11, fra cui una filosofa, Véronique Bonnet.
Grazie a lei ho cominciato a guardare il software libero sotto l'angolo della
filosofia politica. Anche perché mia sorella (Jeanne) m'insegna un pò di
filosofia politica.

Ritorno ai brevetti. Devo precisare che il progetto di brevetto unitario
europeo era una buona idea all'inizio. Ma, sfortunamente, ha dato la
possibilità a vari lobbies (che sono sempre vicino) di spingere verso un
sistema di brevetti all'americana, di cui sto per descrivere alcuni aspetti.

Prima di parlare di brevetti software, si deve parlare della brevettabilità del
software. Ci si può (e ci si deve) interrogare sulla brevettabilità in sé del
software: è pertinente, è legittimo applicare la legislazione dei brevetti nel
ambito del software? Per noi, la risposta è no. Noi non siamo solo attivisti
del software libero. Sono anche delle giurisdizioni come la Nuova Zelanda
(esemplare in questo ambito) e l'Unione Europea dove, in principio, i brevetti
software sono vietati. In pratica, la brevettabilità (o no) del software viene
definita dalla giurisprudenza, poiché il software non esisteva quando i testi
di legge originali vennero scritti.

Si capisce che la questione della brevettabilità del software è politica, e non
soltanto giuridica. All'April, identifichiamo il "microcosmo" dei brevetti. Il
microcosmo dei brevetti viene costituito da tutti gli attori che hanno
interessi nel sistema dei brevetti. Che, a volte, esistano solamente da e
per il sistema dei brevetti. Ricordate l'UEB. Ritroviamo gli studi legali
specializzati nel diritto dei brevetti, le agenzie di concessione, e i grandi
gruppi industriali, che hanno i mezzi per appropriarsi dei mercati, per
accordarsi tra di loro, scambiando portafogli di brevetti, ecc.

Chiaramente non ritroviamo il piccolo genio, che farebbe scoperte nello
scantinato. Questo non esiste, non si fa così, lo sviluppo software. Non
ritroviamo neanche le start-up o le piccole imprese, che non hanno accesso a
servizi di giuristi super qualificati nel diritto dei brevetti. Ma quando si
cercano delle informazioni oppure dei pareri sul sistema dei brevetti, sarà il
microcosmo dei brevetti che vorrà e potrà rispondere. Perché il sistema l'hanno
costruito loro, per loro. Ecco perché vi avverto.

Quindi, c'è un problema, penso, di auto-riferimento ideologico e, inoltre, di
potere finanziario. Forse avete sentito parlare delle guerre di brevetti?
Queste serie di processi che costano millioni di dollari, quando un'impresa
(pensate ai giganti tecnologici) ritiene di essere vittima di violazione di
brevetto ("patent infringement") -- capite, ha bravi avvocati per "dimostrare"
che è vittima... Così può attaccare un concorrente e, magari, fare più soldi
di quanti ne farebbe pubblicando un bel prodotto -- evviva l'innovazione!

Per evitare la guerra aperta (e incessante), le grandi imprese scambiano
portafogli di brevetti tra di loro -- al fine di eludere il sistema dei
brevetti, che impedisce, che ostacola le attività di ricerca e di sviluppo.
Ancora qui, i piccoli attori non possono partecipare a questo gioco. Se sono
perseguiti per violazione di brevetto, non avranno i mezzi per difendersi, che
ci sia violazione di brevetto o no. Ciò ha portato lo sviluppo di un commercio
(un'attività davvero maffiosa), ossia minacciare di processo e far pagare la
gente che fa realmente lo sviluppo software. E proprio estorsione, racket!

Negli Stati Uniti, ci sono centenaia di migliaia di brevetti software. E tante
ditte specializzate che fanno questo racket. Più si consegnano brevetti
software, più si possono sviluppare queste attività di racket. Si dovrebbe
sviluppare il software invece, no? Il microcosmo dei brevetti è molto potente;
pagano lobbisti esperti. Qualunque sia la vostra definizione dell'innovazione,
tradizionalmente è associata alle nozione di emergenza, di competizione, e di
concorrenza: pensate che mantenere posizioni dominanti e intimidere
sviluppatori software (libero o no) sia compatibile con l'idea di innovazione?

In fondo, perché il software deve essere escluso dal campo della
brevettabilità? Perché un software implementa un algoritmo, un metodo, che si
costruisce incrementalmente, mattone su mattone. Non si reinventa la ruota. Non
chiedi prima di programmare qualcosa se qualcuno ha già fatto la stessa cosa,
ha già avuto la stessa idea, perché stai pensando al programma e, comunque, la
risposta è probabilmente "sì", qualcuno l'ha già fatto, perché non è una novità
geniale.

La brevettabilità del software rappresenta una minaccia giuridica e finanziaria
per (tutti) gli sviluppatori di software -- credi di sviluppare un programma in
un angolo, invece forse dovrai pagare diritti esorbitanti. Purtroppo, la
gestione del brevetto unitario europeo viene data all'UEB, che non è
un'istituzione democratica, e che prova continuamente ad estendere la
brevettabilità al settore informatico. Inoltre, ci sono numerosi problemi
legali, economici e politici con questo brevetto unitario.

L'April aveva proposto degli emendamenti per colmare le carenze di democrazia,
di sicurezza giuridica, ecc. Io ho chiamato alcuni parlamentari, mandato
emails... In generale, ho ricevuto una risposta. Un paio di parlamentari sono
anche aperti agli scambi. Quindi, contattate i parlamentari: abbiamo il potere
di influenzarli, sopratutto nel ambito digitale, tecnologico, scientifico
(perché lo conosciamo meglio di loro).

Tuttavia, il Parlamento europeo ha votato per il brevetto unitario alla fine
del 2012. La sua entrata in vigore è stata annunciata per il 2014, 2017 (ma ci
siamo) e, ho visto ieri sul
[sito dell'UEB](https://www.epo.org/law-practice/unitary_fr.html), 2018.
Se l'applicazione si fa mai, l'instabilità delle basi giuridiche (e tanti
problemi) si vedranno presto... Non posso specificare in questo seminario, ma
se siete interessati, vi posso dare delle referenze.

Arrivamo alla fine. A fortiori, la non-brevettabilità del software viene sempre
minacciata quando la sovranità giuridica per sé viene minacciata. È quello che
si sta svolgendo cogli accordi di libero scambio. In Nord-America, abbiamo
[NAFTA](https://act.openmedia.org/NAFTA). Tra l'Europa e gli Stati Uniti, c'è
TAFTA/TTIP, che riciclava ACTA, che i cittadini avevano rifiutato. Tra l'Europa
e il Canada, c'è CETA. E tutti quanti. Allora, questi accordi di libero scambio
minacciano, direttamente o indirettamente, il software libero (e tante altre
cose).

In conclusione, vi invito a mobilitarvi contro questi accordi di libero
scambio, ad utilizzare del software libero, e a creare comunità!
