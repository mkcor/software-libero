# Il software libero all'università, nella comunità e nella vita pubblica: esperienze internazionali

Seminario tenuto il 5 settembre 2017 a Brescia, all'Università Cattolica del Sacro Cuore.

Annuncio ufficiale: http://brescia.unicatt.it/eventi/evt-il-software-libero-all-universita-nella-comunita-e-nella-vita-pubblica-esperienze-internazionali

Annonce en français : https://www.april.org/le-logiciel-libre-l-universite-dans-la-communaute-et-dans-la-vie-publique-experiences-internationale

## Appunti

Sotto `notes/`:

* `00_introduzione.md`
* `01_universita.md`
* `02_comunita.md`
* `03_vitapubblica.md`
